package monitorizare;

import java.io.FileNotFoundException;

public class Main {

	public static Operations op = new Operations();
	
	public static void main(String[] args) {

		
		// 1. Count the distinct days that appear in the monitoring data
		System.out.println("the distinct nr of days is :\t" +op.count());
		
		// 2. Determine a map of type <String, Integer> that maps to each distinct action type the number of
		//occurrences in the log. Write the resulting map into a text file
		try {
			op.occuranceNr();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	
		// 4. Determine a data structure of the form Map<String, DateTime> that maps for each activity the total
		//duration computed over the monitoring period. Filter the activities with total duration larger than
		//10 hours. Write the result in a text file.
		try {
			op.tenHourFilter();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 5. Filter the activities that have 90% of the monitoring samples with duration less than 5 minutes,
		//collect the results in a List<String> containing only the distinct activity names and write the result
		//in a text file
		op.fiveMinuteFilter();
		
	}

}
