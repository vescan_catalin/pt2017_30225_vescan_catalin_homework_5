package monitorizare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.stream.Stream;

public class Operations {
	
	public BufferedReader buff;
	public ArrayList<MonitoredData> list;
	
	public ArrayList<MonitoredData> read (String filePath) {
		String data;
		
		try {
			// citesc din fisier
			buff = new BufferedReader(new FileReader(filePath));
			ArrayList<MonitoredData> ret = new ArrayList<MonitoredData>();
			
			// introduc in lista datele pe care le citesc din fisier
			while((data = buff.readLine()) != null ) 
				ret.add(new MonitoredData(data));
			
			buff.close();
			
			return ret;
		} catch(Exception e) {
			
		}
		return null;
	}

	// stergerea timpului pentru compararea datelor
	public Date scoateTimp(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	// 1. Count the distinct days that appear in the monitoring data
	public int count() {
		list = read("D:/programe java/monitorizare/UCI ADL Binary Dataset/activitati.txt");

		Stream<Object> start = list.stream().map(x -> scoateTimp(x.start_time));
		Stream<Object> end = list.stream().map(x -> scoateTimp(x.end_time));
		
		return (int)Stream.concat(start, end).distinct().count();
	}
	
	// 2. Determine a map of type <String, Integer> that maps to each distinct action type the number of
	//occurrences in the log. Write the resulting map into a text file
	@SuppressWarnings("resource")
	public void occuranceNr() throws FileNotFoundException {			
		File file = new File("rezultat2.txt");
		PrintWriter print = new PrintWriter("rezultat2.txt");
		print.println("rezultat cerinta 2:");
		
		list = read("D:/programe java/monitorizare/UCI ADL Binary Dataset/activitati.txt");
				
		list.stream().map(searchActivity -> searchActivity.activity_label).distinct().map(searchActivity -> {
			return searchActivity +"\t" + list.stream().filter(findActivity -> findActivity.activity_label.compareTo(searchActivity) == 0).count();
		}).forEach(searchActivity -> //System.out.println(searchActivity));
		{
			try {
				if (!file.exists()) 
	                file.createNewFile();
				
				print.println(searchActivity);
				
			} catch(Exception e) {
				
			}
		});
		print.close();
	}
	
	// 3. Generates a data structure of type Map<Integer, Map<String, Integer>> that contains the activity
	//count for each day of the log (task number 2 applied for each day of the log) and writes the result
	//in a text file
	
	// 4. Determine a data structure of the form Map<String, DateTime> that maps for each activity the total
	//duration computed over the monitoring period. Filter the activities with total duration larger than
	//10 hours. Write the result in a text file.
	@SuppressWarnings("resource")
	public void tenHourFilter() throws Exception {
		File file = new File("rezultat4.txt");
		PrintWriter print = new PrintWriter("rezultat4.txt");
		print.println("rezultat cerinta 4:");
		list.stream().map(x -> x.activity_label).distinct().filter(x -> 
		{
			return list.stream().filter(y -> y.activity_label.compareTo(x) == 0).map(y -> y.end_time.getTime() - y.start_time.getTime()).mapToLong(y -> y.longValue()).sum() >= 10 * 60 * 60 * 1000;			
		}).forEach(x -> //System.out.println(searchActivity));
		{
			try {
				if (!file.exists()) 
	                file.createNewFile();
								
				print.println(x);
				
			} catch(Exception e) {
				
			}
		});
		print.close();
	}
	
	// 5. Filter the activities that have 90% of the monitoring samples with duration less than 5 minutes,
	//collect the results in a List<String> containing only the distinct activity names and write the result
	//in a text file
	public void fiveMinuteFilter() {		
		File file = new File("rezultat5.txt");
		
		list.stream().map(x -> x.activity_label).distinct().filter(x -> 
		{
			long mai_mic = list.stream().filter(y -> y.activity_label.compareTo(x) == 0).filter(y -> y.end_time.getTime() - y.start_time.getTime() <= 5 * 60 * 1000).count();
			long total = list.stream().filter(y -> y.activity_label.compareTo(x) == 0).count();
			return mai_mic * 100 / total >= 90;
		}).forEach(x -> //System.out.println(searchActivity));
		{
			try {
				if (!file.exists()) 
	                file.createNewFile();
	                
				PrintWriter print = new PrintWriter("rezultat5.txt");
				print.println("rezultat cerinta 5:");
				print.println(x);
				print.close();
			} catch(Exception e) {
				
			}
		});
	}
	
}
