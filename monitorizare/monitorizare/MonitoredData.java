package monitorizare;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {

	Date start_time, end_time;
	String activity_label = "";

	public MonitoredData(String data) {
		String[] fields = data.split("\t\t");

		try {
			start_time = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(fields[0]);
			end_time = new SimpleDateFormat("yyy-mm-dd hh:mm:ss").parse(fields[1]);
		} catch (Exception e) {
			e.printStackTrace();
		}

		activity_label = fields[2];
		// System.out.println(activityLabel);
	}

}
